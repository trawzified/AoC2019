#include<iostream>
#include<string>
#include<vector>

using std::cout;
using std::endl;
using std::vector;
using std::string;

vector<int> process_intcodes(vector<int> intcodes) {
	for(int i = 0; i >= 0; i += 4) {
		switch(intcodes[i]) {
			case 1:
				// Addition
				intcodes[intcodes[i+3]] = intcodes[intcodes[i+1]] + intcodes[intcodes[i+2]];
				break;
			case 2:
				// Multiply
				intcodes[intcodes[i+3]] = intcodes[intcodes[i+1]] * intcodes[intcodes[i+2]];
				break;
			case 99:
				// Terminate
				i = -99;
				//cout << "Exitcode 99";
				break;
			default:
				cout << "Error! " << intcodes[i] << " is not a valid opcode.";
				i = -99;
				break;
		}
	}
	return intcodes;
}

int main() {
	vector<vector<int>> all_intcodes = {{1, 0, 0, 0, 99}, {2, 3, 0, 3, 99}, {2, 4, 4, 5, 99, 0}, {1, 1, 1, 4, 99, 5, 6, 0, 99}};
	for(vector<int> intcodes : all_intcodes) {
		cout << "Original intcode: ";
		for(int intcode : intcodes) {
			cout << intcode << " ";
		}
		cout << endl;
		cout << "Processed intcode: ";
		for(int intcode : process_intcodes(intcodes)) {
			cout << intcode << " ";
		}
		cout << endl;
		cout << endl;
	}
	return 0;
}
