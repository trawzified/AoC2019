#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using std::cout;
using std::vector;
using std::string;
using std::stoi;
using std::to_string;

char field[10][11];

template<int rows, int cols>
void print_2d_array(char(&array)[rows][cols]) {
	for(int i = 0; i < rows; ++i) {
		for(int j = 0; j < cols; ++j) {
			cout << array[i][j] << '\t';
		}
		cout << '\n';
	}
}

template<int rows, int cols>
void fill_2d_array_with_char(char(&array)[rows][cols], char c) {
	for(int i = 0; i < rows; ++i) {
		for(int j = 0; j < cols; ++j) {
			array[i][j] = c;
		}
	}
}

vector<int> get_next_pos(vector<int> curr_pos, string transition) {
	vector<int> next_pos = curr_pos;
	if(transition.size() >= 2) {
		int num = stoi(transition.substr(1, transition.size() - 1));
		if(num > 0) {
			switch(transition[0]) {
				case 'R':
					next_pos[0] += num;
					break;
				case 'L':
					next_pos[0] -= num;
					break;
				case 'U':
					next_pos[1] += num;
					break;
				case 'D':
					next_pos[1] -= num;
					break;
				default:
					cout << "ERROR: invalid transition \'" << transition << "\'\n";
					break;
			}
		}
		else {
			return next_pos;
		}
	}
	else {
		cout << "ERROR: transition too short\n";
	}
}

vector<vector<int>> get_wire_for_transitions(vector<string> transitions) {
	vector<vector<int>> wire_positions = {{0, 0}};
	for(string transition : transitions) {
		// Apply transition to last element in vector and push it at the end
		int num = stoi(transition.substr(1, transition.size() - 1));
		int i = 1;
		vector<int> last_wire_pos = wire_positions[wire_positions.size() - 1];
		while(num > i) {
			wire_positions.push_back(get_next_pos(last_wire_pos, transition[0] + to_string(i)));
			i++;
		}
	}
	return wire_positions;
}

void print_wire(vector<vector<int>> wire) {
	for(vector<int> x : wire) {
		cout << x[0] << ", " << x[1] << '\n';
	}
}

vector<vector<int>> get_intersection_for_wires(vector<vector<int>> wire1, vector<vector<int>> wire2) {
	vector<vector<int>> intersections;
	for(vector<int> wire1_pos : wire1) {
		// if wire1_pos is in wire2
		if(std::find(wire2.begin(), wire2.end(), wire1_pos) != wire2.end()) {
			intersections.push_back(wire1_pos);
		}
	}
	return intersections;
}

int get_distance_to_first(vector<int> position) {
	return std::abs(position[0]) + std::abs(position[1]);
}

int main() {
	//fill_2d_array_with_char(field, '.');
	//print_2d_array(field);
	vector<string> wire1_transitions = {"R8", "U5", "L5", "D3"};
	vector<string> wire2_transitions = {"U7", "R6", "D4", "L4"};

	vector<string> wire3_transitions = {"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"};
	vector<string> wire4_transitions = {"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"};

	vector<vector<int>> wire1 = get_wire_for_transitions(wire1_transitions);
	vector<vector<int>> wire2 = get_wire_for_transitions(wire2_transitions);

	vector<vector<int>> wire3 = get_wire_for_transitions(wire3_transitions);
	vector<vector<int>> wire4 = get_wire_for_transitions(wire4_transitions);

	/* cout << "Wire 3:\n"; */
	/* print_wire(wire3); */
	/* cout << "Wire 4:\n"; */
	/* print_wire(wire4); */
	cout << "Wire Intersections:\n";
	vector<vector<int>> wire_intersections = get_intersection_for_wires(wire3, wire4);
	print_wire(wire_intersections);
	cout << "Min Wire Distance:\n";
	int min_dist = get_distance_to_first(wire_intersections[1]);
	for(vector<int> intersection : wire_intersections) {
		int dist = get_distance_to_first(intersection);
		if(dist < min_dist && dist != 0) {
			min_dist = dist;
		}
	}
	cout << min_dist;

	
	vector<string> wire5_transitions = {"R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"};
	vector<string> wire6_transitions = {"U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"};

	vector<vector<int>> wire5 = get_wire_for_transitions(wire5_transitions);
	vector<vector<int>> wire6 = get_wire_for_transitions(wire6_transitions);
	vector<vector<int>> wire_intersections2 = get_intersection_for_wires(wire5, wire6);
	
	min_dist = get_distance_to_first(wire_intersections2[1]);
	for(vector<int> intersection : wire_intersections) {
		int dist = get_distance_to_first(intersection);
		if(dist < min_dist && dist != 0) {
			min_dist = dist;
		}
	}
	cout << "Min Wire Distance:\n";
	cout << min_dist;
	return 0;
}
