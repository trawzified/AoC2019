#include<iostream>
#include<vector>
#include<cmath>

int get_fuel_for_mass(int mass) {
	return floor(mass / 3) - 2;
}
int main() {
	int amount_modules = 0;
	std::cout << "How many modules are there?" << std::endl;
	std::cin >> amount_modules;
	std::vector<int> module_fuels = {};
	for(int i = 0; i < amount_modules; i++) {
		int input = 0;
		std::cout << "Input for mass: " << i << std::endl;
		std::cin >> input;
		module_fuels.push_back(get_fuel_for_mass(input));
	}

	int total_fuel = 0;
	for(int fuel : module_fuels) {
		total_fuel += fuel;
	}
	std::cout << "Total amount of fuel required:" << total_fuel << std::endl;
	
	return 0;
}
